# Android Project - FootballFixtures
Create Football Fixtures app that consumes fixtures data from https://www.footballdata.org

## Choice of Architecture - MVVM

It enforces Separation of concerns & has the benefits of easier testing and modularity. As activity is never responsible for the requests for fetching data, the ViewModels can handle that efficiently.

ViewModels are classes that interacts with the logic/model layer and just exposes states/data and actually has no idea by whom or how that data will be consumed. Only View(Activity) holds the reference to ViewModel and not vice versa, this solves our tight coupling issue. A single view can hold reference to multiple ViewModels.

## This Application is developed entirely in Kotlin

## Libraries used

* Google AppCompat Support Libraries
* Google Material Design Support Libraries
* Glide - for image loading
* Anko library - for fast and type-safe dynamic Android layouts
* AndroidSvg - for loading SVG images with Glide internally
* Retrofit2 - type-safe REST client to make API request.
* Kotlin Coroutines - for async callbacks, managing background threads & long-running tasks such as network requests
* MOSHI - as my Retrofit adapter & for parsing the JSON responses
* Okhttp - used effectively for logging network errors
* JUnit - for Unit testing
* Mockito - for creating a "mock" for classes
* Espresso - for User Interface testing


## Building and running the project

* Android Studio 3.2.1
* Android Gradle Plugin 3.2.1


## Other things done.

* Themed app according to specification in the design
* Implemented effective Error Management, checked possible response codes and display the appropriate errors

## Please note:
Some resources are restricted to my footballdata.org user account subscription, app would show appropriate error response from footballdata.org server.

Example: `The resource you are looking for is restricted, please check your subscription for permission`

- Premier league has no restrictions with the auth token on this project



