package com.example.footballfixtures.utils.util

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.util.Log
import com.example.footballfixtures.R
import com.example.footballfixtures.utils.Constants
import org.jetbrains.anko.runOnUiThread
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    private const val TAG = "Utils"

    fun showDialog(context: Context, title: String, message: String) {
        context.runOnUiThread {
            try {
                val alertDialog = AlertDialog.Builder(context, R.style.DialogTheme).create()
                alertDialog.setTitle(title)
                alertDialog.setMessage(getMessageSpan(context, message))
                alertDialog.setButton(
                        AlertDialog.BUTTON_POSITIVE,
                        getButtonTextSpan(context, context.getString(R.string.back))
                ) { dialog, _ -> dialog.dismiss() }
                alertDialog.show()
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).
                        setTextColor(ContextCompat.getColor(context, R.color.colorDialogPositiveButton))
            } catch (e: Exception) {
                Log.e(TAG, "Exception --- > " + e.message)
            }
        }
    }

    private fun getMessageSpan(context: Context, message: String): SpannableStringBuilder {
        val messageString = SpannableStringBuilder(message)
        messageString.setSpan(
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorDialogText)),
                0,
                message.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

        return messageString
    }

    private fun getButtonTextSpan(context: Context, text: String): SpannableStringBuilder {
        return SpannableStringBuilder(text)
    }

    fun toTimeHourMin(cleanStr: String): String {

        val DATE_TIME_ONLY = SimpleDateFormat(Constants.outputFormatTime, Locale.getDefault())
        var clean = cleanStr
        if (clean != "") {
            try {
                val parseDate = SimpleDateFormat(Constants.inputFormat, Locale.getDefault()).parse(clean)
                clean = DATE_TIME_ONLY.format(parseDate)

            } catch (e: ParseException) {
                Log.e("FormatFormDate", Log.getStackTraceString(e))
            }
        }
        return clean
    }
}