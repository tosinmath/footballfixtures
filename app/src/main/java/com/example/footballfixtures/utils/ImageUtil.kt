package com.example.footballfixtures.utils

import android.app.Activity
import android.content.Context
import android.widget.ImageView
import com.ahmadrosid.svgloader.SvgLoader
import com.bumptech.glide.Glide


class ImageUtil{
    companion object {
        fun displayImage(context: Context, imageResource: String, imageView: ImageView, drawable: Int){
            Glide.with(context)
                    .load(imageResource)
                    .placeholder(drawable)
                    .into(imageView)
        }

        fun displaySVG(context: Activity, imageResource: String, imageView: ImageView, drawable: Int){
            SvgLoader.pluck()
                    .with(context)
                    .setPlaceHolder(drawable, drawable)
                    .load(imageResource, imageView)
        }
    }
}