package com.example.footballfixtures.utils

internal object Constants {
    const val LEAGUE_CODE = "leagueCode"
    const val LEAGUE_NAME = "leagueName"
    const val TEAM_ID = "teamid"
    const val TEAM_NAME = "teamName"
    const val TEAM_IMAGE_URL = "teamImageUrl"
    const val inputFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val outputFormatTime = "HH:mm"
}