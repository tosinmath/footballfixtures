package com.example.footballfixtures.repository

import android.arch.lifecycle.MutableLiveData
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.ui.base.FeedResource
import kotlinx.coroutines.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import retrofit2.Response

fun <RESPONSE : Any> fetchFeeds(liveData: MutableLiveData<FeedResource<RESPONSE>>, block: FeedsCallHandler<RESPONSE>.() -> Unit) {
    return FeedsCallHandler<RESPONSE>().apply(block).makeCall(liveData)
}

class FeedsCallHandler<RESPONSE : Any> : AnkoLogger {
    lateinit var client: Deferred<Response<ApiResponse>>

    fun makeCall(result: MutableLiveData<FeedResource<RESPONSE>>) {
        result.value = FeedResource.Loading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = client.await()
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        result.value = FeedResource.Loading(false)
                        result.value = FeedResource.Success(response.body()!!)
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        result.value = FeedResource.Loading(false)
                        result.value = FeedResource.Error(response.code(), response.errorBody()!!)
                    }
                }

            } catch (ex: Exception) {
                error("Error occurred when making network call", ex)
                withContext(Dispatchers.Main) {
                    result.value = FeedResource.Loading(false)
                    result.value = FeedResource.Exception()
                }
            }
        }
    }
}