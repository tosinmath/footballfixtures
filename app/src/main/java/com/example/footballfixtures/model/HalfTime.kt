package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class HalfTime(

	@Json(name="awayTeam")
	val awayTeam: Int,

	@Json(name="homeTeam")
	val homeTeam: Int
)