package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class ApiResponse(

		@field:Json(name="count")
	val count: Int,

		@field:Json(name="competition")
	val competition: Competition,

		@field:Json(name="competitions")
	val competitionsItem: List<CompetitionsItem>,

		@Json(name="teams")
	val teams: List<TeamsItem>,

		@Json(name="standings")
	val standings: List<StandingsItem>,

		@field:Json(name="filters")
	val filters: Filters,

		@field:Json(name="matches")
	val matches: List<MatchesItem>,

	/* Players response */

		@Json(name="crestUrl")
	val crestUrl: String? = null,

		@Json(name="squad")
	val squad: List<SquadItem>,

		@Json(name="name")
	val name: String? = null
)