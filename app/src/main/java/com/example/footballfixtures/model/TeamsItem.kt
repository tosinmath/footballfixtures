package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class TeamsItem(

		@Json(name="area")
	val area: Area? = null,

		@Json(name="venue")
	val venue: String? = null,

		@Json(name="website")
	val website: String? = null,

		@Json(name="address")
	val address: String? = null,

		@Json(name="crestUrl")
	val crestUrl: String? = null,

		@Json(name="tla")
	val tla: String? = null,

		@Json(name="founded")
	val founded: Int? = null,

		@Json(name="lastUpdated")
	val lastUpdated: String? = null,

		@Json(name="clubColors")
	val clubColors: String? = null,

		@Json(name="phone")
	val phone: String? = null,

		@Json(name="name")
	val name: String? = null,

		@Json(name="id")
	val id: Int? = null,

		@Json(name="shortName")
	val shortName: String? = null,

		@Json(name="email")
	val email: String? = null
)