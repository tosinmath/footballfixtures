package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Competition(

	@Json(name="area")
	val area: Area? = null,

	@Json(name="lastUpdated")
	val lastUpdated: String? = null,

	@Json(name="code")
	val code: Any? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="plan")
	val plan: String? = null
)