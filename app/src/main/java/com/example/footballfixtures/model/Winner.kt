package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Winner(

	@Json(name="crestUrl")
	val crestUrl: Any? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="tla")
	val tla: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="shortName")
	val shortName: String? = null
)