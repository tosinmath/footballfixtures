package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Score(

	@Json(name="duration")
	val duration: String,

	@Json(name="winner")
	val winner: String,

	@Json(name="penalties")
	val penalties: Any,

	@Json(name="halfTime")
	val halfTime: Any,

	@Json(name="fullTime")
	val fullTime: FullTime,

	@Json(name="extraTime")
	val extraTime: Any
)