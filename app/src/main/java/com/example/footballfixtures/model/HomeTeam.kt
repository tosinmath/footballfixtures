package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class HomeTeam(

	@Json(name="name")
	val name: String,

	@Json(name="id")
	val id: Int
)