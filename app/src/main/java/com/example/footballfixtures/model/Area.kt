package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Area(

	@Json(name="name")
	val name: String? = null,

	@Json(name="id")
	val id: Int? = null
)