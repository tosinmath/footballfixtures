package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class ErrorResponse(

	@Json(name="errorCode")
	val errorCode: Int,

	@Json(name="message")
	val message: String
)