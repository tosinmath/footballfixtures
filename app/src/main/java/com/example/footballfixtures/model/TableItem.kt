package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class TableItem(

		@Json(name="goalsFor")
	val goalsFor: Int,

		@Json(name="lost")
	val lost: Int,

		@Json(name="won")
	val won: Int,

		@Json(name="playedGames")
	val playedGames: Int,

		@Json(name="position")
	val position: Int? = null,

		@Json(name="team")
	val team: Team,

		@Json(name="draw")
	val draw: Int,

		@Json(name="goalsAgainst")
	val goalsAgainst: Int,

		@Json(name="goalDifference")
	val goalDifference: Int,

		@Json(name="points")
	val points: Int
)