package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class CompetitionsItem(

	@Json(name="area")
	val area: Area? = null,

	@Json(name="lastUpdated")
	val lastUpdated: String? = null,

	@Json(name="code")
	val code: String? = null,

	@Json(name="emblemUrl")
	val emblemUrl: Any? = null,

	@Json(name="currentSeason")
	val currentSeason: CurrentSeason? = null,

	@Json(name="name")
	val name: String? = null,

	@field:Json(name="id")
	val id: Int,

	@Json(name="numberOfAvailableSeasons")
	val numberOfAvailableSeasons: Int? = null,

	@Json(name="plan")
	val plan: String? = null
)