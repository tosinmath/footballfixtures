package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class CurrentSeason(

	@Json(name="winner")
	val winner: Any? = null,

	@Json(name="currentMatchday")
	val currentMatchday: Any? = null,

	@Json(name="endDate")
	val endDate: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="startDate")
	val startDate: String? = null
)