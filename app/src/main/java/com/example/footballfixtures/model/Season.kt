package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Season(

	@Json(name="winner")
	val winner: Any? = null,

	@Json(name="currentMatchday")
	val currentMatchday: Int,

	@Json(name="endDate")
	val endDate: String,

	@Json(name="id")
	val id: Int,

	@Json(name="startDate")
	val startDate: String
)