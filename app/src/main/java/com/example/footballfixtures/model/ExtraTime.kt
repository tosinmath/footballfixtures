package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class ExtraTime(

	@Json(name="awayTeam")
	val awayTeam: Any,

	@Json(name="homeTeam")
	val homeTeam: Any
)