package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Filters(
	val any: Any
)