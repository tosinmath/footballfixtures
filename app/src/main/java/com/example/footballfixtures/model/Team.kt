package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class Team(

	@Json(name="crestUrl")
	val crestUrl: String? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="id")
	val id: Int? = null
)