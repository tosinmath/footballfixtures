package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class RefereesItem(

	@Json(name="nationality")
	val nationality: Any,

	@Json(name="name")
	val name: String,

	@Json(name="id")
	val id: Int
)