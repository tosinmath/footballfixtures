package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class StandingsItem(

		@Json(name="stage")
	val stage: String,

		@Json(name="type")
	val type: String,

		@Json(name="table")
	val table: List<TableItem>,

		@Json(name="group")
	val group: Any
)