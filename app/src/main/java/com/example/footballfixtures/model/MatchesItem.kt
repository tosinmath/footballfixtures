package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class MatchesItem(

        @Json(name="lastUpdated")
	val lastUpdated: String? = null,

        @Json(name="competition")
	val competition: Competition? = null,

        @Json(name="score")
	val score: Score? = null,

        @Json(name="stage")
	val stage: String? = null,

        @Json(name="matchday")
	val matchday: Int? = null,

        @Json(name="awayTeam")
	val awayTeam: AwayTeam? = null,

        @Json(name="season")
	val season: Season? = null,

        @Json(name="homeTeam")
	val homeTeam: HomeTeam? = null,

        @Json(name="id")
	val id: Int? = null,

        @Json(name="utcDate")
	val utcDate: String? = null,

        @Json(name="referees")
	val referees: List<RefereesItem?>? = null,

        @Json(name="status")
	val status: String? = null,

        @Json(name="group")
	val group: String? = null
)