package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class SquadItem(

	@Json(name="role")
	val role: String? = null,

	@Json(name="nationality")
	val nationality: String? = null,

	@Json(name="countryOfBirth")
	val countryOfBirth: String? = null,

	@Json(name="shirtNumber")
	val shirtNumber: Int? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="dateOfBirth")
	val dateOfBirth: String? = null,

	@Json(name="id")
	val id: Int? = null,

	@Json(name="position")
	val position: String? = null
)