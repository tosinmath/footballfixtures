package com.example.footballfixtures.model

import com.squareup.moshi.Json

data class FullTime(

	@Json(name="awayTeam")
	val awayTeam: Int? = null,

	@Json(name="homeTeam")
	val homeTeam: Int? = null
)