package com.example.footballfixtures.network

import com.example.footballfixtures.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

object AuthorizationInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
                .newBuilder()
                .addHeader("X-Auth-Token", BuildConfig.AUTH_TOKEN)
                .addHeader("Content-Type", "application/json")
                .build()

        return chain.proceed(request)
    }
}