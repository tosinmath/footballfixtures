package com.example.footballfixtures.network

import com.example.footballfixtures.model.ApiResponse
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("/v2/matches?dateFrom=2018-12-18&dateTo=2018-12-18")
    fun listMatchesCall(): Call<ApiResponse>

    @GET("/v2/matches")
    fun listMatches(): Deferred<Response<ApiResponse>>

    @GET("/v2/competitions")
    fun listCompetitions(): Deferred<Response<ApiResponse>>

    @GET("/v2/competitions/{league}/standings")
    fun listLeagueTables(@Path("league") league: String): Deferred<Response<ApiResponse>>

    @GET("/v2/competitions/{league}/matches")
    fun listLeagueFixtures(@Path("league") league: String): Deferred<Response<ApiResponse>>

    @GET("/v2/competitions/{league}/teams")
    fun listLeagueTeams(@Path("league") league: String): Deferred<Response<ApiResponse>>

    @GET("/v2/teams/{teamId}")
    fun listPlayerList(@Path("teamId") teamId: Int): Deferred<Response<ApiResponse>>

}