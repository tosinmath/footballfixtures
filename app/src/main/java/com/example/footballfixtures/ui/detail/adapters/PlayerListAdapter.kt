package com.example.footballfixtures.ui.detail.adapters

import android.content.Context
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.SquadItem
import com.example.footballfixtures.ui.list.adapters.PlayerListViewHolder

class PlayerListAdapter(val context: Context, val mItem: MutableList<SquadItem>?): RecyclerView.Adapter<PlayerListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerListViewHolder {
        return PlayerListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.player_name_list, parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(@NonNull holder: PlayerListViewHolder, position: Int) {
        val model = mItem!![holder.adapterPosition]

        holder.apply {
            val pos = position + 1
            this.numbers.text = pos.toString()
            model.name?.let{ this.name.text = model.name }
            model.position?.let{ this.position.text = model.position.toString() }
        }
    }

    override fun getItemCount(): Int{
        return mItem!!.size
    }
}