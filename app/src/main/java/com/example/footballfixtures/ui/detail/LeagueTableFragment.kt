package com.example.footballfixtures.ui.detail

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.model.StandingsItem
import com.example.footballfixtures.ui.base.BaseFragment
import com.example.footballfixtures.ui.base.FeedResource
import com.example.footballfixtures.ui.detail.adapters.TableAdapter
import com.example.footballfixtures.utils.Constants
import com.example.footballfixtures.utils.observe
import com.example.footballfixtures.utils.withViewModel
import kotlinx.android.synthetic.main.layout_no_content.*
import kotlinx.android.synthetic.main.layout_recyclerview.*

class LeagueTableFragment : BaseFragment() {

    private lateinit var adapter: TableAdapter
    private lateinit var viewModel: LeagueViewModel
    private var leagueCode = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_league_table, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null && arguments!!.containsKey(Constants.LEAGUE_CODE)) {
            leagueCode = arguments!!.getString(Constants.LEAGUE_CODE)!!
        }

        if(!leagueCode.isEmpty()){
            viewModel = withViewModel<LeagueViewModel> {
                observe(feedResponse) { handleSuccessResult(it) }
            }
            viewModel.getLeagueTables(leagueCode)
        }
    }

    private fun handleSuccessResult(FeedResource: FeedResource<ApiResponse>?) {
        when (FeedResource) {
            is FeedResource.Loading -> handleLoading(FeedResource.isLoading)
            is FeedResource.Error -> handleResponseError(FeedResource.httpStatusCode, FeedResource.errorBody.string())
            is FeedResource.Exception -> handleResponseException()
            is FeedResource.Success -> {
                val list = FeedResource.response
                displayTables(list.standings)
            }
        }
    }

    fun displayTables(itemList: List<StandingsItem>){
        handleLoading(false)
        if(!itemList.isEmpty()) {
            val table = itemList.get(0).table
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            recyclerViewStandings.addItemDecoration(decoration)
            adapter = TableAdapter(this.requireActivity(), activity!!.applicationContext, table.toMutableList())
            recyclerViewStandings.adapter = adapter
        } else {
            showRetryButton()
        }
    }

    override fun showRetryButton(){
        layoutNoContent.visibility = View.VISIBLE
        noContentText.text = getString(R.string.no_result)
        noContentButton.setOnClickListener {
            viewModel.getLeagueTables(leagueCode)
            layoutNoContent.visibility = View.GONE
        }
    }
}