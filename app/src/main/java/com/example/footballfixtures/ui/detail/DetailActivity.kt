package com.example.footballfixtures.ui.detail

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.Toolbar
import android.view.View
import com.example.footballfixtures.R
import com.example.footballfixtures.ui.base.BaseActivity
import com.example.footballfixtures.utils.Constants
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : BaseActivity() {

    private var leagueCode = "";
    private var leagueName = "";
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        toolbar = findViewById<View>(R.id.toolbar_main) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val extras = intent.extras
        if(extras != null){
            leagueCode = extras.getString(Constants.LEAGUE_CODE)!!
            if(!leagueCode.isEmpty()) {

                // set the argument bundle
                val bundle = Bundle()
                bundle.putString(Constants.LEAGUE_CODE, leagueCode)

                container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
                tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))

                val leagueTableFragment = LeagueTableFragment()
                leagueTableFragment.arguments = bundle

                val leagueFixturesFragment = LeagueFixturesFragment()
                leagueFixturesFragment.arguments = bundle

                val leagueTeamsFragment = LeagueTeamsFragment()
                leagueTeamsFragment.arguments = bundle

                val fragments = listOf(leagueTableFragment, leagueFixturesFragment, leagueTeamsFragment)
                val sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, fragments)
                container.adapter = sectionsPagerAdapter
                container.offscreenPageLimit = 3;
            }

            leagueName = extras.getString(Constants.LEAGUE_NAME)!!
            if(!leagueName.isEmpty()) {
                supportActionBar!!.title = leagueName
            }
        }
    }
}
