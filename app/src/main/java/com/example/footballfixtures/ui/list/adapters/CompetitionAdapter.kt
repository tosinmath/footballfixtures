package com.example.footballfixtures.ui.list.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.CompetitionsItem
import com.example.footballfixtures.ui.detail.DetailActivity
import com.example.footballfixtures.utils.Constants.LEAGUE_CODE
import com.example.footballfixtures.utils.Constants.LEAGUE_NAME
import com.example.footballfixtures.utils.util.Utils

class CompetitionAdapter(val context: Context, val mItem: MutableList<CompetitionsItem>?): RecyclerView.Adapter<CompetitionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompetitionViewHolder {
        return CompetitionViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.competition_list, parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(@NonNull holder: CompetitionViewHolder, position: Int) {
        val model = mItem!![holder.adapterPosition]

        var code = ""
        var name = ""

        holder.apply {

            model.name?.let{
                this.name.text = model.name
                name = model.name
            }
            model.code?.let{ code = model.code }
            model.currentSeason?.let{
                val startDate = model.currentSeason.startDate
                val endDate = model.currentSeason.endDate
                if(!startDate.isNullOrEmpty() && !endDate.isNullOrEmpty()){
                    name = name + " " + startDate.substring(0, 4) + "/" + endDate.substring(0, 2)
                }
            }
        }

        holder.itemView.setOnClickListener { v->
            if(!code.isEmpty()) {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra(LEAGUE_CODE, code)
                intent.putExtra(LEAGUE_NAME, name)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                val activity = v.context as Activity
                activity.startActivity(intent)
            } else {
                Utils.showDialog(
                        context, context.getString(R.string.error_title) ,
                        context.getString(R.string.league_code_missing)
                )
            }
        }
    }

    override fun getItemCount(): Int{
        return mItem!!.size
    }
}