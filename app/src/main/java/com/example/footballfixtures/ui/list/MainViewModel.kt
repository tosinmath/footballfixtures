package com.example.footballfixtures.ui.list

import android.arch.lifecycle.MutableLiveData
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.network.ApiClient
import com.example.footballfixtures.repository.fetchFeeds
import com.example.footballfixtures.ui.base.BaseViewModel
import com.example.footballfixtures.ui.base.FeedResource

class MainViewModel : BaseViewModel() {

    val feedResponse = MutableLiveData<FeedResource<ApiResponse>>()

    fun getAllMatches() {
        fetchFeeds(feedResponse) {
            client = ApiClient.apiService.listMatches()
        }
    }

    fun getAllCompetitions() {
        fetchFeeds(feedResponse) {
            client = ApiClient.apiService.listCompetitions()
        }
    }
}