package com.example.footballfixtures.ui.list

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.model.MatchesItem
import com.example.footballfixtures.ui.base.BaseFragment
import com.example.footballfixtures.ui.base.FeedResource
import com.example.footballfixtures.ui.list.adapters.FixtureAdapter
import com.example.footballfixtures.utils.observe
import com.example.footballfixtures.utils.withViewModel
import kotlinx.android.synthetic.main.fragment_fixtures.*
import kotlinx.android.synthetic.main.layout_no_content.*

class FixturesFragment : BaseFragment() {

    private lateinit var adapter: FixtureAdapter
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_fixtures, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(activity != null) {
            (activity as MainActivity).setToolbarTitle(getString(R.string.title_fixtures))
        }

        viewModel = withViewModel<MainViewModel> {
            observe(feedResponse) { handleSuccessResult(it) }
        }
        viewModel.getAllMatches()
    }

    private fun handleSuccessResult(FeedResource: FeedResource<ApiResponse>?) {
        when (FeedResource) {
            is FeedResource.Loading -> handleLoading(FeedResource.isLoading)
            is FeedResource.Error -> handleResponseError(FeedResource.httpStatusCode, FeedResource.errorBody.string())
            is FeedResource.Exception -> handleResponseException()
            is FeedResource.Success -> {
                val list = FeedResource.response
                displayFeeds(list.matches)
            }
        }
    }

    fun displayFeeds(itemList: List<MatchesItem>){
        handleLoading(false)
        if(!itemList.isEmpty()) {
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            recyclerFixtures.addItemDecoration(decoration)
            adapter = FixtureAdapter(context!!, itemList.toMutableList())
            recyclerFixtures.adapter = adapter
        } else {
            showRetryButton()
        }
    }

    companion object {
        fun newInstance(): FixturesFragment = FixturesFragment()
    }

    override fun showRetryButton(){
        layoutNoContent.visibility = View.VISIBLE
        noContentText.text = getString(R.string.no_fixtures)
        noContentButton.setOnClickListener {
            viewModel.getAllMatches()
            layoutNoContent.visibility = View.GONE
        }
    }
}