package com.example.footballfixtures.ui.detail.adapters

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.TeamsItem
import com.example.footballfixtures.ui.detail.PlayerListFragment
import com.example.footballfixtures.ui.list.adapters.TeamsViewHolder
import com.example.footballfixtures.utils.Constants
import com.example.footballfixtures.utils.ImageUtil
import com.example.footballfixtures.utils.util.Utils


class TeamsAdapter(val activity: Activity, val context: Context, val mItem: MutableList<TeamsItem>?): RecyclerView.Adapter<TeamsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamsViewHolder {
        return TeamsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.teams_grid_list, parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(@NonNull holder: TeamsViewHolder, position: Int) {
        val model = mItem!![holder.adapterPosition]

        var teamId = -1;
        var teamName = "";
        var url = "";

        holder.apply {

            model.crestUrl?.let {
                url = model.crestUrl
                val parts = url.split(".")
                if(!parts.isNullOrEmpty()) {
                    val extension = parts[parts.size - 1]
                    if (extension.equals("svg")) {

                        ImageUtil.displaySVG(activity, model.crestUrl, this.crestUrl,
                                R.drawable.place_holder)
                    } else {
                        ImageUtil.displayImage(context, model.crestUrl, this.crestUrl,
                                R.drawable.place_holder)
                    }
                }
            }

            model.name?.let{
                this.name.text = model.name
                teamName = model.name
            }
            model.id?.let{ teamId = model.id }

            holder.itemView.setOnClickListener { v->
                if(teamId != -1) {

                    val bundle = Bundle()
                    bundle.putInt(Constants.TEAM_ID, teamId)
                    bundle.putString(Constants.TEAM_NAME, teamName)
                    bundle.putString(Constants.TEAM_IMAGE_URL, url)

                    val playerListFragment = PlayerListFragment()
                    playerListFragment.arguments = bundle
                    playerListFragment.show((context as AppCompatActivity).supportFragmentManager,
                            playerListFragment.getTag())

                } else {
                    Utils.showDialog(
                            context, context.getString(R.string.error_title) ,
                            context.getString(R.string.team_id_missing)
                    )
                }
            }



        }
    }

    override fun getItemCount(): Int{
        return mItem!!.size
    }
}