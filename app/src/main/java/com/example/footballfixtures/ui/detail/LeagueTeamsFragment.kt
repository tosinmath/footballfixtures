package com.example.footballfixtures.ui.detail

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.model.TeamsItem
import com.example.footballfixtures.ui.base.BaseFragment
import com.example.footballfixtures.ui.base.FeedResource
import com.example.footballfixtures.ui.detail.adapters.TeamsAdapter
import com.example.footballfixtures.utils.Constants.LEAGUE_CODE
import com.example.footballfixtures.utils.observe
import com.example.footballfixtures.utils.withViewModel
import kotlinx.android.synthetic.main.layout_no_content.*
import kotlinx.android.synthetic.main.layout_recyclerview.*

class LeagueTeamsFragment : BaseFragment() {

    private lateinit var adapter: TeamsAdapter
    private lateinit var viewModel: LeagueViewModel
    private var leagueCode = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_league_teams, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null && arguments!!.containsKey(LEAGUE_CODE)) {
            leagueCode = arguments!!.getString(LEAGUE_CODE)!!
        }

        if(!leagueCode.isEmpty()) {
            viewModel = withViewModel<LeagueViewModel> {
                observe(feedResponse) { handleSuccessResult(it) }
            }
            viewModel.getLeagueTeams(leagueCode)
        }
    }

    private fun handleSuccessResult(FeedResource: FeedResource<ApiResponse>?) {
        when (FeedResource) {
            is FeedResource.Loading -> handleLoading(FeedResource.isLoading)
            is FeedResource.Error -> handleResponseError(FeedResource.httpStatusCode, FeedResource.errorBody.string())
            is FeedResource.Exception -> handleResponseException()
            is FeedResource.Success -> {
                val list = FeedResource.response
                displayTeams(list.teams)
            }
        }
    }

    fun displayTeams(itemList: List<TeamsItem>){
        handleLoading(false)
        if(!itemList.isEmpty()) {
            val layoutManager: RecyclerView.LayoutManager
            layoutManager = GridLayoutManager(requireActivity(), 3)
            recyclerViewStandings.setLayoutManager(layoutManager)
            adapter = TeamsAdapter(requireActivity(), context!!, itemList.toMutableList())
            recyclerViewStandings.adapter = adapter
            recyclerViewStandings.setPadding(10, 40, 0, 0);
        } else {
            showRetryButton()
        }
    }

    override fun showRetryButton(){
        layoutNoContent.visibility = View.VISIBLE
        noContentText.text = getString(R.string.no_result)
        noContentButton.setOnClickListener {
            viewModel.getLeagueTables(leagueCode)
            layoutNoContent.visibility = View.GONE
        }
    }
}