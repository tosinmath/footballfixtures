package com.example.footballfixtures.ui.list.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.footballfixtures.R

class FixtureViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val md by lazy { view.findViewById(R.id.md) as TextView }
    val utcDate by lazy { view.findViewById(R.id.time) as TextView }
    val status by lazy { view.findViewById(R.id.status) as TextView }
    val score1 by lazy { view.findViewById(R.id.score1) as TextView }
    val score2 by lazy { view.findViewById(R.id.score2) as TextView }
    val homeTeamTitle by lazy { view.findViewById(R.id.homeTeam) as TextView }
    val awayTeamTitle by lazy { view.findViewById(R.id.awayTeam) as TextView }
}

class CompetitionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val name by lazy { view.findViewById(R.id.name) as TextView }
}

class TeamsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val crestUrl by lazy { view.findViewById(R.id.crestUrl) as ImageView }
    val name by lazy { view.findViewById(R.id.name) as TextView }
}

class StandingsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val crestUrl by lazy { view.findViewById(R.id.crestUrl) as ImageView }
    val numbers by lazy { view.findViewById(R.id.numbers) as TextView }
    val name by lazy { view.findViewById(R.id.name) as TextView }
    val goalDifference by lazy { view.findViewById(R.id.goalDifference) as TextView }
    val goalsFor by lazy { view.findViewById(R.id.goalsFor) as TextView }
    val points by lazy { view.findViewById(R.id.points) as TextView }
}

class PlayerListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val numbers by lazy { view.findViewById(R.id.numbers) as TextView }
    val name by lazy { view.findViewById(R.id.name) as TextView }
    val position by lazy { view.findViewById(R.id.position) as TextView }
}