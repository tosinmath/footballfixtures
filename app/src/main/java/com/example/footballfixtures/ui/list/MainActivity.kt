package com.example.footballfixtures.ui.list

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.View
import com.example.footballfixtures.R
import com.example.footballfixtures.ui.base.BaseActivity
import com.example.footballfixtures.utils.widget.BottomNavigationBehavior

class MainActivity : BaseActivity() {

    private lateinit var toolbar: Toolbar

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val fixturesFragment = FixturesFragment.newInstance()
                openFragment(fixturesFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                val competitionFragment = CompetitionFragment.newInstance()
                openFragment(competitionFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById<View>(R.id.toolbar_main) as Toolbar
        setSupportActionBar(toolbar)

        val navigation = findViewById<View>(R.id.navigation) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        // attaching bottom sheet behaviour - hide / show on scroll
        val layoutParams = navigation.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.behavior = BottomNavigationBehavior()

        val fixturesFragment = FixturesFragment.newInstance()
        openFragment(fixturesFragment)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun setToolbarTitle(title: String){
        if(toolbar != null) {
            supportActionBar!!.title = title
        }
    }
}
