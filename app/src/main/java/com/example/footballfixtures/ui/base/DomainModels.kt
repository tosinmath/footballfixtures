package com.example.footballfixtures.ui.base

import com.example.footballfixtures.model.ApiResponse
import okhttp3.ResponseBody

sealed class FeedResource<T> {
    class Success<T>(val response: ApiResponse) : FeedResource<T>() {
        override fun toString(): String {
            return "FeedResource.Success{response=$response}"
        }
    }
    class Error<T>(val httpStatusCode: Int, val errorBody: ResponseBody) : FeedResource<T>()
    class Loading<T>(val isLoading: Boolean) : FeedResource<T>()
    class Exception<T> : FeedResource<T>()
}