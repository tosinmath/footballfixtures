package com.example.footballfixtures.ui.detail

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.model.SquadItem
import com.example.footballfixtures.ui.base.BaseBottomSheetFragment
import com.example.footballfixtures.ui.base.FeedResource
import com.example.footballfixtures.ui.detail.adapters.PlayerListAdapter
import com.example.footballfixtures.utils.Constants
import com.example.footballfixtures.utils.ImageUtil
import com.example.footballfixtures.utils.observe
import com.example.footballfixtures.utils.withViewModel
import kotlinx.android.synthetic.main.fragment_player_list.*
import kotlinx.android.synthetic.main.layout_no_content.*
import kotlinx.android.synthetic.main.layout_recyclerview.*


class PlayerListFragment : BaseBottomSheetFragment() {

    private lateinit var adapter: PlayerListAdapter
    private lateinit var viewModel: LeagueViewModel
    private var teamId = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_player_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationIcon(R.drawable.close_button)

        if (arguments != null && arguments!!.containsKey(Constants.TEAM_ID)) {
            teamId = arguments!!.getInt(Constants.TEAM_ID)
        }

        if (arguments != null && arguments!!.containsKey(Constants.TEAM_NAME)) {
            toolbar.title = arguments!!.getString(Constants.TEAM_NAME)
        }

        if (arguments != null && arguments!!.containsKey(Constants.TEAM_IMAGE_URL)) {
            showTeamImage(arguments!!.getString(Constants.TEAM_IMAGE_URL)!!)
        }

        if(teamId != -1) {
            viewModel = withViewModel<LeagueViewModel> {
                observe(feedResponse) { handleSuccessResult(it) }
            }
            viewModel.getPlayerList(teamId)
        }

        toolbar.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction().remove(this).commit();
        }
    }

    private fun handleSuccessResult(FeedResource: FeedResource<ApiResponse>?) {
        when (FeedResource) {
            is FeedResource.Loading -> handleLoading(FeedResource.isLoading)
            is FeedResource.Error -> handleResponseError(FeedResource.httpStatusCode, FeedResource.errorBody.string())
            is FeedResource.Exception -> handleResponseException()
            is FeedResource.Success -> {
                val list = FeedResource.response
                displayLeagueFixtures(list.squad)
            }
        }
    }

    fun displayLeagueFixtures(itemList: List<SquadItem>){
        handleLoading(false)
        if(!itemList.isEmpty()) {
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            recyclerViewStandings.addItemDecoration(decoration)
            adapter = PlayerListAdapter(context!!, itemList.toMutableList())
            recyclerViewStandings.adapter = adapter
        } else {
            showRetryButton()
        }
    }

    override fun showRetryButton(){
        layoutNoContent.visibility = View.VISIBLE
        noContentText.text = getString(R.string.no_result)
        noContentButton.setOnClickListener {
            viewModel.getPlayerList(teamId)
            layoutNoContent.visibility = View.GONE
        }
    }

    private fun showTeamImage(url: String){
        val parts = url.split(".")
        if(!parts.isNullOrEmpty()) {
            val extension = parts[parts.size - 1]
            if (extension.equals("svg")) {
                ImageUtil.displaySVG(requireActivity(), url, teamImage,
                        R.drawable.place_holder)
            } else {
                ImageUtil.displayImage(context!!, url, teamImage,
                        R.drawable.place_holder)
            }
        }
    }
}