package com.example.footballfixtures.ui.detail

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.model.MatchesItem
import com.example.footballfixtures.ui.base.BaseFragment
import com.example.footballfixtures.ui.base.FeedResource
import com.example.footballfixtures.ui.list.adapters.FixtureAdapter
import com.example.footballfixtures.utils.Constants
import com.example.footballfixtures.utils.observe
import com.example.footballfixtures.utils.withViewModel
import kotlinx.android.synthetic.main.layout_no_content.*
import kotlinx.android.synthetic.main.layout_recyclerview.*

class LeagueFixturesFragment : BaseFragment() {

    private lateinit var adapter: FixtureAdapter
    private lateinit var viewModel: LeagueViewModel
    private var leagueCode = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_league_fixtures, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null && arguments!!.containsKey(Constants.LEAGUE_CODE)) {
            leagueCode = arguments!!.getString(Constants.LEAGUE_CODE)!!
        }

        if(!leagueCode.isEmpty()) {
            viewModel = withViewModel<LeagueViewModel> {
                observe(feedResponse) { handleSuccessResult(it) }
            }
            viewModel.getLeagueFixtures(leagueCode)
        }
    }

    private fun handleSuccessResult(FeedResource: FeedResource<ApiResponse>?) {
        when (FeedResource) {
            is FeedResource.Loading -> handleLoading(FeedResource.isLoading)
            is FeedResource.Error -> handleResponseError(FeedResource.httpStatusCode, FeedResource.errorBody.string())
            is FeedResource.Exception -> handleResponseException()
            is FeedResource.Success -> {
                val list = FeedResource.response
                displayLeagueFixtures(list.matches)
            }
        }
    }

    fun displayLeagueFixtures(itemList: List<MatchesItem>){
        handleLoading(false)
        if(!itemList.isEmpty()) {
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            recyclerViewStandings.addItemDecoration(decoration)
            adapter = FixtureAdapter(context!!, itemList.toMutableList())
            recyclerViewStandings.adapter = adapter
        } else {
            showRetryButton()
        }
    }

    override fun showRetryButton(){
        layoutNoContent.visibility = View.VISIBLE
        noContentText.text = getString(R.string.no_fixtures)
        noContentButton.setOnClickListener {
            viewModel.getLeagueTables(leagueCode)
            layoutNoContent.visibility = View.GONE
        }
    }
}