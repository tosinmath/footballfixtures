package com.example.footballfixtures.ui.base

import android.support.v4.app.Fragment
import android.util.Log
import com.example.footballfixtures.R
import com.example.footballfixtures.utils.util.Utils
import com.example.footballfixtures.utils.widget.Progressbar
import org.json.JSONObject


abstract class BaseFragment: Fragment() {

    private lateinit var progressbar: Progressbar

    companion object {
        private const val TAG = "BaseFragment"
    }

    private fun showProgressBar() {
        progressbar = Progressbar.show(requireActivity())
    }

    private fun hideProgressBar() {
        try {
            if (::progressbar.isInitialized) {
                progressbar.dismiss()
            }
        } catch (e: Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
        }
    }

    fun handleLoading(loading: Boolean){
        if(loading){
            showProgressBar()
        } else {
            hideProgressBar()
        }
    }

    fun handleResponseError(code: Int, errorBody: String) {
        val activity = requireActivity()
        showRetryButton()
        when (code) {
            400, 401, 422, 403 -> {
                Utils.showDialog(activity, getString(R.string.error_title) , getErrorMessage(errorBody))
            }
            504 -> Utils.showDialog(activity, getString(R.string.connection_timeout_title) , getString(R.string.connection_timeout_title))
            else -> Utils.showDialog(activity, getString(R.string.error_title) , getString(R.string.something_went_wrong))
        }
    }

    fun handleResponseException() {
        Utils.showDialog(requireActivity(), getString(R.string.error_title) , getString(R.string.connection_no_network))
        showRetryButton()
    }

    abstract fun showRetryButton()

    private fun getErrorMessage(errorBody: String): String {
        val errorObject = JSONObject(errorBody)
        return errorObject.getString("message")
    }
}