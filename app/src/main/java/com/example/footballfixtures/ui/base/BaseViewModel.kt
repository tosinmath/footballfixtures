package com.example.footballfixtures.ui.base

import android.arch.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()