package com.example.footballfixtures.ui.list.adapters

import android.content.Context
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.MatchesItem
import com.example.footballfixtures.utils.util.Utils.toTimeHourMin

class FixtureAdapter(val context: Context, val mItem: MutableList<MatchesItem>?): RecyclerView.Adapter<FixtureViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FixtureViewHolder {
        return FixtureViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.fixtures_list, parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(@NonNull holder: FixtureViewHolder, position: Int) {
        val model = mItem!![holder.adapterPosition]

        holder.apply {
            this.score1.text = "0"
            model.score?.let{
                if(model.score.fullTime.homeTeam != null){
                    this.score1.text = model.score.fullTime.homeTeam.toString()
                }
            }
            model.score?.let{
                this.score2.text = "0"
                if(model.score.fullTime.awayTeam != null){
                    this.score2.text = model.score.fullTime.awayTeam.toString()
                }
            }
            model.status?.let{ this.status.text = model.status }
            model.matchday?.let{
                val md = "MD: " + model.matchday.toString()
                this.md.text = md
            }
            model.utcDate?.let{
                this.utcDate.text = toTimeHourMin(model.utcDate)
            }
            model.homeTeam?.let{ this.homeTeamTitle.text = model.homeTeam.name }
            model.awayTeam?.let{ this.awayTeamTitle.text = model.awayTeam.name }
        }
    }

    override fun getItemCount(): Int{
        return mItem!!.size
    }
}