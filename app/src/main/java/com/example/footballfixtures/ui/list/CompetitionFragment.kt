package com.example.footballfixtures.ui.list

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.model.CompetitionsItem
import com.example.footballfixtures.ui.base.BaseFragment
import com.example.footballfixtures.ui.base.FeedResource
import com.example.footballfixtures.ui.list.adapters.CompetitionAdapter
import com.example.footballfixtures.utils.observe
import com.example.footballfixtures.utils.withViewModel
import kotlinx.android.synthetic.main.fragment_competition.*
import kotlinx.android.synthetic.main.layout_no_content.*

class CompetitionFragment : BaseFragment() {

    private lateinit var adapter: CompetitionAdapter
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_competition, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(activity != null) {
            (activity as MainActivity).setToolbarTitle(getString(R.string.title_competition))
        }

        viewModel = withViewModel<MainViewModel> {
            observe(feedResponse) { handleSuccessResult(it) }
        }
        viewModel.getAllCompetitions()
    }

    private fun handleSuccessResult(FeedResource: FeedResource<ApiResponse>?) {
        when (FeedResource) {
            is FeedResource.Loading -> handleLoading(FeedResource.isLoading)
            is FeedResource.Error -> handleResponseError(FeedResource.httpStatusCode, FeedResource.errorBody.string())
            is FeedResource.Exception -> handleResponseException()
            is FeedResource.Success -> {
                val list = FeedResource.response
                displayCompetition(list.competitionsItem)
            }
        }
    }

    fun displayCompetition(itemList: List<CompetitionsItem>){
        handleLoading(false)
        if(!itemList.isEmpty()) {
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            recyclerCompetition.addItemDecoration(decoration)
            adapter = CompetitionAdapter(context!!, itemList.toMutableList())
            recyclerCompetition.adapter = adapter
        } else {
            showRetryButton()
        }
    }

    companion object {
        fun newInstance(): CompetitionFragment = CompetitionFragment()
    }

    override fun showRetryButton(){
        layoutNoContent.visibility = View.VISIBLE
        noContentText.text = getString(R.string.no_competition)
        noContentButton.setOnClickListener {
            viewModel.getAllMatches()
            layoutNoContent.visibility = View.GONE
        }
    }
}

