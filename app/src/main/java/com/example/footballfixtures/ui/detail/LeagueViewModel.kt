package com.example.footballfixtures.ui.detail

import android.arch.lifecycle.MutableLiveData
import com.example.footballfixtures.model.ApiResponse
import com.example.footballfixtures.network.ApiClient
import com.example.footballfixtures.repository.fetchFeeds
import com.example.footballfixtures.ui.base.BaseViewModel
import com.example.footballfixtures.ui.base.FeedResource

class LeagueViewModel : BaseViewModel() {

    val feedResponse = MutableLiveData<FeedResource<ApiResponse>>()

    fun getLeagueTables(leagueCode: String) {
        fetchFeeds(feedResponse) {
            client = ApiClient.apiService.listLeagueTables(leagueCode)
        }
    }

    fun getLeagueFixtures(leagueCode: String) {
        fetchFeeds(feedResponse) {
            client = ApiClient.apiService.listLeagueFixtures(leagueCode)
        }
    }

    fun getLeagueTeams(leagueCode: String) {
        fetchFeeds(feedResponse) {
            client = ApiClient.apiService.listLeagueTeams(leagueCode)
        }
    }

    fun getPlayerList(teamId: Int) {
        fetchFeeds(feedResponse) {
            client = ApiClient.apiService.listPlayerList(teamId)
        }
    }
}