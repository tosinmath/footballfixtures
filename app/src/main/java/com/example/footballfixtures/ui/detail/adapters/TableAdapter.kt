package com.example.footballfixtures.ui.detail.adapters

import android.app.Activity
import android.content.Context
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.footballfixtures.R
import com.example.footballfixtures.model.TableItem
import com.example.footballfixtures.ui.list.adapters.StandingsViewHolder
import com.example.footballfixtures.utils.ImageUtil

class TableAdapter(val activity: Activity, val context: Context, val mItem: MutableList<TableItem>?): RecyclerView.Adapter<StandingsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StandingsViewHolder {
        return StandingsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.table_list, parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(@NonNull holder: StandingsViewHolder, position: Int) {
        val model = mItem!![holder.adapterPosition]

        holder.apply {

            model.position?.let{ this.numbers.text = model.position.toString() }
            model.team.name.let{ this.name.text = model.team.name }
            model.goalDifference.let{ this.goalDifference.text = model.goalDifference.toString() }
            model.points.let{ this.points.text = model.points.toString() }

            model.team.crestUrl?.let {
                val url = model.team.crestUrl
                val parts = url.split(".")
                if(!parts.isNullOrEmpty()) {
                    val extension = parts[parts.size - 1]
                    if (extension.equals("svg")) {

                        ImageUtil.displaySVG(activity, model.team.crestUrl, this.crestUrl,
                                R.drawable.place_holder)
                    } else {
                        ImageUtil.displayImage(context, model.team.crestUrl, this.crestUrl,
                                R.drawable.place_holder)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int{
        return mItem!!.size
    }
}